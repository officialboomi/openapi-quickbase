# QuickBase Connector
A low-code application development platform.

Documentation: https://developer.quickbase.com/

Specification: https://github.com/Enspire-Tech/openapi-connector-artifacts/blob/master/quickbase/custom-specification-quickbase.yaml

## Prerequisites

+ Quickbase account
+ API token

## Supported Operations

**3 out of 35 endpoints failing.**

The following operations are **not** supported at this time:
* deleteApp
* deleteFields
* deleteRecords


## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

